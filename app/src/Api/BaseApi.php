<?php

namespace MEA\Platform\Api;

use MEA\Platform\Traits\ConfigOptions;

abstract class BaseApi
{
    use ConfigOptions;

    protected $client;

    abstract public function initClient();

    public function __construct(array $config = null)
    {
        $this->setConfig($config ?: []);
    }

    public function getClient()
    {
        $this->initClient();

        return $this->client;
    }

    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }
}
