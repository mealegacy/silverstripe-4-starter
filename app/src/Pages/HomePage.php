<?php

namespace MEA\Platform\Pages;

use MEA\Platform\Controllers\HomePageController;
use Page;

class HomePage extends Page
{
    private static $db = [];

    private static $has_one = [];

    public function getControllerName()
    {
        return HomePageController::class;
    }
}
