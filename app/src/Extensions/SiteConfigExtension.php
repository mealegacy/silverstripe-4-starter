<?php

namespace MEA\Platform\Extensions;

use MEA\Platform\Helpers\FormHelper;
use Page;
use SilverStripe\Assets\Image;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Core\Convert;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HeaderField;
use SilverStripe\Forms\Tab;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TreeDropdownField;
use SilverStripe\ORM\DataExtension;
use SilverStripe\SiteConfig\SiteConfig;

class SiteConfigExtension extends DataExtension
{
    private static $db = [
        'FooterCopyrightText' => 'Varchar(255)',
        'GoogleAnalyticsTrackingID' => 'Varchar(255)',
        'FacebookPixelID' => 'Varchar(255)',
        'ContactEmail' => 'Varchar(255)',
        'ContactPhone' => 'Varchar(255)',
        'OpenGraphURL' => 'Varchar(255)',
        'OpenGraphTitle' => 'Varchar(255)',
        'OpenGraphLocale' => 'Varchar(255)',
        'OpenGraphDescription' => 'Varchar(255)',
    ];

    private static $has_one = [
        'SiteLogo' => Image::class,
        'FooterLogo' => Image::class,
        'ContactPage' => Page::class,
        'OpenGraphImage' => Image::class,
    ];

    public function updateCMSFields(FieldList $fields)
    {
        // CONTACT TAB
        $contactOptionsTab = Tab::create('TabContactOptions', 'Contact');
        $fields->insertAfter('Main', $contactOptionsTab);

        // Contact fields
        $contactOptionsTab->push(TreeDropdownField::create('ContactPageID', 'Contact Page', SiteTree::class));
        $contactOptionsTab->push(EmailField::create('ContactEmail', 'Contact Email'));
        $contactOptionsTab->push(TextField::create('ContactPhone', 'Contact Phone'));

        // THEME TAB
        $themeOptionsTab = Tab::create('TabThemeOptions', 'Theme');
        $fields->insertAfter('TabContactOptions', $themeOptionsTab);

        // Header content
        $themeOptionsTab->push(HeaderField::create('HeaderOptions', 'Header Options'));

        // Site Logos
        $themeOptionsTab->push(FormHelper::UploadField('SiteLogo', 'Site Logo', ['svg']));

        // Footer content
        $themeOptionsTab->push(HeaderField::create('FooterOptions', 'Footer Options'));

        // Footer copyright text
        $themeOptionsTab->push(TextField::create('FooterCopyrightText', 'Footer Copyright Text'));

        // Footer logo
        $themeOptionsTab->push(FormHelper::UploadField('FooterLogo', 'Footer Logo', ['svg']));

        // INTEGRATIONS TAB
        $integrationsTab = Tab::create('TabIntegrations', 'Integrations');
        $fields->insertAfter('TabThemeOptions', $integrationsTab);

        // Analytics
        $integrationsTab->push(HeaderField::create('GoogleAnalyticsIntegration', 'Analytics'));
        $integrationsTab->push(TextField::create('GoogleAnalyticsTrackingID', 'Google Analytics Tracking ID'));
        $integrationsTab->push(TextField::create('FacebookPixelID', 'Facebook Pixel ID'));

        // Open Graph
        $integrationsTab->push(HeaderField::create('OpenGraphIntegration', 'Open Graph'));
        $integrationsTab->push(TextField::create('OpenGraphTitle', 'Site Title'));
        $integrationsTab->push(TextField::create('OpenGraphURL', 'Site URL'));

        $openGraphLocalField = TextField::create('OpenGraphLocale', 'Site Locale');
        $openGraphLocalField->setDescription('eg: \'en_US\', \'en_NZ\' etc');
        $integrationsTab->push($openGraphLocalField);

        $integrationsTab->push(FormHelper::UploadField('OpenGraphImage', 'Site Logo/Image Thumbnail'));

        $openGraphDescriptionField = TextareaField::create('OpenGraphDescription', 'Site Description');
        $openGraphDescriptionField->setAttribute('maxlength', 250);
        $openGraphDescriptionField->setDescription('Maximum 250 characters.');
        $integrationsTab->push($openGraphDescriptionField);
    }

    public function OpenGraphTags()
    {
        $config = SiteConfig::current_site_config();
        $openGraphFields = [
            'og:title' => 'OpenGraphTitle',
            'og:url' => 'OpenGraphURL',
            'og:locale' => 'OpenGraphLocale',
            'og:description' => 'OpenGraphDescription',
            'og:image' => 'OpenGraphImageID',
        ];
        $openGraphTagsHtml = '';

        // Init required tags
        foreach ($openGraphFields as $tagName => $dbFieldName) {
            if (!$config->getField($dbFieldName)) {
                continue;
            }

            switch ($tagName) {
                case 'og:image': {
                    $openGraphImageUrl = $config->OpenGraphImage()->getAbsoluteURL();
                    $openGraphTagsHtml .= '<meta property="'.Convert::raw2att($tagName).'" content="'.Convert::raw2att($openGraphImageUrl).'" />'."\n";

                    break;
                }
                default: {
                    $openGraphTagsHtml .= '<meta property="'.Convert::raw2att($tagName).'" content="'.Convert::raw2att($config->$dbFieldName).'" />'."\n";
                }
            }
        }

        // Init some sensible defaults
        $openGraphTagsHtml .= '<meta property="og:type" content="website" />';

        return $openGraphTagsHtml;
    }
}
