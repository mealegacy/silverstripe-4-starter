<?php

namespace MEA\Platform\Extensions;

use SilverStripe\Control\Director;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\ToggleCompositeField;
use SilverStripe\ORM\DataExtension;

class PageExtension extends DataExtension
{
    private static $db = [
        'Subtitle' => 'Varchar(255)',
        'HeaderSnippets' => 'HTMLText',
        'FooterSnippets' => 'HTMLText',
    ];

    public function updateCMSFields(FieldList $fields)
    {
        // Remove unnecessary fields
        $subtitleField = TextField::create('Subtitle', 'Page Subtitle');
        $fields->insertAfter('Title', $subtitleField);

        // SNIPPETS
        $snippetsField = ToggleCompositeField::create('SnippetsHeader', 'Snippets', []);
        $fields->insertAfter('Metadata', $snippetsField);

        // Header snippets
        $headerSnippetsField = TextareaField::create('HeaderSnippets', 'Header Snippets');
        $headerSnippetsField->setRows(5);
        $headerSnippetsField->setDescription('HTML tags to go just above the &lt;/head&gt; tag of the page.');
        $snippetsField->insertAfter('SnippetsHeader', $headerSnippetsField);

        // Footer snippets
        $footerSnippetsField = TextareaField::create('FooterSnippets', 'Header Snippets');
        $footerSnippetsField->setRows(5);
        $footerSnippetsField->setDescription('HTML tags to go just above the &lt;/body&gt; tag on the page.');
        $snippetsField->insertAfter('HeaderSnippets', $footerSnippetsField);

        return $fields;
    }

    public function CurrentDate($format = 'd M, Y')
    {
        return date($format);
    }

    public function CurrentYear()
    {
        return date('Y');
    }

    public function LoginLink($backUrl = null)
    {
        $loginLink = 'login';

        if ($backUrl) {
            $loginLink .= '?BackURL='.urlencode($backUrl);
        }

        return $this->getSecurityLink($loginLink);
    }

    public function LogoutLink()
    {
        return $this->getSecurityLink('logout');
    }

    protected function getSecurityLink($action)
    {
        return Director::absoluteBaseURL().'Security/'.$action;
    }
}
