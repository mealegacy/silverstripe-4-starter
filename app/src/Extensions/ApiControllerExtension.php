<?php

namespace MEA\Platform\Extensions;

use SilverStripe\Core\Extension;

class ApiControllerExtension extends Extension
{
    const RESPONSE_STATUS_ERROR = 'error';
    const RESPONSE_STATUS_SUCCESS = 'success';

    /**
     * Get JSON response for given data.
     *
     * @param mixed  $data       data to encode
     * @param string $status     JSON response status
     * @param int    $statusCode
     *
     * @return string JSON response
     */
    public function getJsonResponse($data, $status = self::RESPONSE_STATUS_SUCCESS, $statusCode = 200)
    {
        $this->owner->getResponse()->addHeader('Content-Type', 'application/json');
        $this->owner->getResponse()->setStatusCode($statusCode);
        $this->owner->getResponse()->setBody(json_encode([
            'status' => $status,
            'data' => $data,
        ]));

        return $this->owner->getResponse();
    }

    /**
     * Get JSON error response for given data.
     *
     * @param mixed  $data       data to encode
     * @param string $status     JSON response status
     * @param int    $statusCode
     *
     * @return string JSON response
     */
    public function getJsonErrorResponse($data, $status = self::RESPONSE_STATUS_ERROR, $statusCode = 400)
    {
        return $this->owner->getJsonResponse($data, $status, $statusCode);
    }
}
