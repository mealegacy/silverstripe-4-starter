<?php

namespace MEA\Platform\Controllers;

use SilverStripe\CMS\Controllers\ContentController;

class HomePageController extends ContentController
{
    private static $allowed_actions = [];
}
