<?php

namespace MEA\Platform\Helpers;

use Psr\SimpleCache\CacheInterface;
use SilverStripe\Core\Injector\Injector;

class CacheHelper
{
    const DEFAULT_CACHE_TITLE = 'SiteCache';

    protected $cache;
    protected $cacheKey;

    public function __construct($cacheKey = null)
    {
        $this->cacheKey = $cacheKey;
    }

    public function getValue($key = null, $default = null)
    {
        if ($key) {
            if (!$this->getCache()->has($key)) {
                return $default;
            }

            return $this->getCache()->get($key);
        }

        return $this->getCache();
    }

    public function setValue($key, $value)
    {
        $this->getCache()->set($key, $value);

        return $this;
    }

    protected function getCache()
    {
        if (null === $this->cache) {
            $cacheKey = $this->getCacheKey() ?: static::DEFAULT_CACHE_TITLE;
            $this->cache = Injector::inst()->get(CacheInterface::class.'.'.$cacheKey);
        }

        return $this->cache;
    }

    protected function getCacheKey()
    {
        return $this->cacheKey;
    }
}
