<?php

namespace MEA\Platform\Helpers;

use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\GridField\GridFieldButtonRow;
use SilverStripe\Forms\GridField\GridFieldConfig;
use SilverStripe\Forms\GridField\GridFieldDeleteAction;
use SilverStripe\Forms\GridField\GridFieldFilterHeader;
use SilverStripe\Forms\GridField\GridFieldPaginator;
use SilverStripe\Forms\GridField\GridFieldSortableHeader;
use SilverStripe\Forms\GridField\GridFieldToolbarHeader;
use Symbiote\GridFieldExtensions\GridFieldAddNewInlineButton;
use Symbiote\GridFieldExtensions\GridFieldEditableColumns;

class FormHelper
{
    /**
     * Helper to create an Image Upload field.
     *
     * @param $fieldName
     * @param null  $fieldTitle
     * @param array $extraAllowedExtensions
     * @param bool  $allowMultipleFiles
     *
     * @return UploadField
     */
    public static function UploadField(
        $fieldName,
        $fieldTitle = null,
        $extraAllowedExtensions = [],
        $allowMultipleFiles = false
    ) {
        $allowedExtensions = array_merge([
            'jpg',
            'jpeg',
            'png',
        ], $extraAllowedExtensions);

        $uploadField = UploadField::create($fieldName, $fieldTitle ?: $fieldName);
        $uploadField->setIsMultiUpload($allowMultipleFiles);
        $uploadField->setAllowedExtensions($extraAllowedExtensions);
        $uploadField->setDescription('Allowed file types: '.implode(', ', $allowedExtensions));

        return $uploadField;
    }

    public static function getInlineEditableGridFieldConfig($addNewTitle = 'Add New Item')
    {
        $gridFieldConfig = GridFieldConfig::create();
        $gridFieldConfig->addComponent(new GridFieldButtonRow('before'));

        $gridFieldConfig->addComponent(new GridFieldToolbarHeader());
        $gridFieldConfig->addComponent(new GridFieldFilterHeader());
        $gridFieldConfig->addComponent(new GridFieldSortableHeader());
        $gridFieldConfig->addComponent(new GridFieldEditableColumns());
        $gridFieldConfig->addComponent(new GridFieldDeleteAction());
        $gridFieldConfig->addComponent(new GridFieldPaginator());

        $addNewButton = new GridFieldAddNewInlineButton();
        $addNewButton->setTitle($addNewTitle);
        $gridFieldConfig->addComponent($addNewButton);

        return $gridFieldConfig;
    }
}
