<?php

namespace MEA\Platform\Traits;

use SilverStripe\View\ArrayData;

trait ConfigOptions
{
    protected $config;

    public function getConfig($option = null, $default = null): ArrayData
    {
        if ($this->config === null) {
            $this->config = ArrayData::create([]);
        }

        if ($this->config && $option) {
            $value = $this->config->getField($option);
            if (!$value) {
                return $default;
            }

            return $value;
        }

        return $this->config;
    }

    public function setConfig(array $config)
    {
        $this->config = ArrayData::create($config);

        return $this;
    }

    public function addConfig($key, $value)
    {
        $this->config->setField($key, $value);

        return $this;
    }
}