<?php

$finder = PhpCsFixer\Finder::create()
	->files()
	->in(__DIR__.'/app')
	->notPath('src/Symfony/Component/Translation/Tests/fixtures/resources.php')
	->ignoreDotFiles(true)
	->ignoreVCS(true);

return PhpCsFixer\Config::create()
	->setFinder($finder)
	->setLineEnding("\n")
	->setRules([
		'@Symfony' => true,
		'array_syntax' => [
			'syntax' => 'short',
		],
		'blank_line_before_return' => true,
		'no_unneeded_curly_braces' => false,
	]);
