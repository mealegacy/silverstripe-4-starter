// Load packages vars
const pkg = require('./package.json');

// Load packages
const gulp = require('gulp');
const path = require('path');

// Load Gulp plugins
const $ = require("gulp-load-plugins")({
  pattern: ["*"],
  scope: ["devDependencies"]
});

// Helper to convert JSON value to boolean
const getBoolean = (str) => str.trim().toLowerCase() === 'true';

// Helper function for logging errors
const logError = (error) => console.error(`-> ${error}`);

const banner = `/**
  * @project        ${pkg.name}
  * @author         ${pkg.author}
  * @build          ${$.moment().format('llll')} NZDT
  * @release        ${$.gitRevSync.branch()}#${$.gitRevSync.long()}
  * @copyright      Copyright (c) ${$.moment().format('YYYY')} ${pkg.copyright}
  */
`;

// Default tasks
gulp.task('default', ['fonts', 'css', 'js']);

gulp.task('watch', ['default'], () => {
  gulp.watch([path.join(pkg.paths.scss, '**/*.scss')], ['css']);
  gulp.watch([path.join(pkg.paths.js, '**/*.js')], ['js']);
});

// CSS tasks
gulp.task('css', () => {
  // CSS Nano config
  const cssNanoConfig = {
    discardComments: {
      removeAll: true
    },
    discardDuplicates: true,
    discardEmpty: true,
    minifyFontValues: true,
    minifySelectors: true
  };
  
  pkg.blobs.css.map((blob) => {
    return gulp.src(blob.src)
      .pipe($.plumber({errorHandler: logError}))
      .pipe($.sass({includePaths: pkg.paths.scss}).on("error", $.sass.logError))
      .pipe($.concat(path.basename(blob.dest)))
      .pipe($.if($.util.env.production, $.cssnano(cssNanoConfig)))
      .pipe($.header(banner, {pkg: pkg}))
      .pipe($.size({showFiles: true}))
      .pipe(gulp.dest(path.dirname(blob.dest)));
  });
});

// Javascript tasks
gulp.task('js', () => {
  // Babel config
  const babelConfig = {
    plugins: [],
    presets: ['env']
  };
  if ($.util.env.production) {
    babelConfig.compact = true;
    babelConfig.presets.push("minify");
    babelConfig.plugins.push(["transform-remove-console", {"exclude": ["error", "warn"]}]);
  }
  
  pkg.blobs.js.map((blob) => {
    return gulp.src(blob.src)
      .pipe($.plumber({errorHandler: logError}))
      .pipe($.concat(path.basename(blob.dest)))
      .pipe($.if($.util.env.production, $.stripComments({trim: true})))
      .pipe($.if(getBoolean(blob.bundle), $.babel(babelConfig)))
      .pipe($.header(banner, {pkg: pkg}))
      .pipe($.size({showFiles: true}))
      .pipe(gulp.dest(path.dirname(blob.dest)));
  });
});

// Font tasks
gulp.task('fonts', ['fontello']);

// Generate Fontello icon font
gulp.task('fontello', () => {
  // Generate Fontello CSS and fonts
  gulp.src(path.join(pkg.paths.fontello, 'config.json'))
    .pipe($.fontello({font: 'fonts'}))
    .pipe($.size({showFiles: true, showTotal: false}))
    .pipe(gulp.dest(pkg.paths.fontello));
  
  // Copy generated font files to dist directory
  gulp.src(path.join(pkg.paths.fontello, '/fonts/*.{eot,svg,ttf,woff,woff2}'))
    .pipe(gulp.dest(pkg.paths.fonts));
  
  // Concat and copy generated CSS files to dist directory
  gulp.src(path.join(pkg.paths.fontello, '/css/*.css'))
    .pipe($.concat('_fontello.scss'))
    .pipe($.size({showFiles: true, showTotal: false}))
    .pipe(gulp.dest(path.join(pkg.paths.scss, 'libs')));
});
