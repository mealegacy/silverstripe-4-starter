#!/usr/bin/env bash

# Typography
COLOR_RED='\033[1;32m'
COLOR_INITIAL='\033[0m'

# Deployment paths
SRC_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DEST_PATH=
DEST_HOST=
DEST_USER=

# Pre-deployment
printf "${COLOR_RED}> Compiling static assets${COLOR_INITIAL}\n"
gulp --production

# Sync project files
printf "\n${COLOR_RED}> Syncing files between local and server${COLOR_INITIAL}\n"
pushd $SRC_PATH/../

# Copy Silverstripe Environgment file, but not replace the one on the server
rsync --compress --ignore-existing --checksum --info=progress2 \
    .env.remote $DEST_USER@$DEST_HOST:$DEST_PATH/.env

# Copy .htaccess, but not replace the one on the server
rsync --compress --ignore-existing --checksum --info=progress2 \
    .htaccess $DEST_USER@$DEST_HOST:$DEST_PATH

# Copy assets folder the first time around
rsync --compress --recursive --ignore-existing --checksum --info=progress2 \
    assets $DEST_USER@$DEST_HOST:$DEST_PATH

# Copy remaining project files
rsync --compress --recursive --checksum --info=progress2 \
    --exclude .git \
    --exclude bower_components \
    --exclude node_modules \
    --exclude assets \
    --exclude .babelrc \
    --exclude .gitignore \
    --exclude .editorconfig \
    --exclude .htaccess \
    --exclude .php_cs \
    --exclude .php_cs.cache \
    --exclude *.env \
    --exclude gulpfile.js \
    --exclude package.json \
    --exclude package-lock.json \
    --exclude phpcs.xml.dist \
    . $DEST_USER@$DEST_HOST:$DEST_PATH
popd

# Run DB migrations, and flush cache and templates
printf "\n${COLOR_RED}> Running DB migrations, and flushing cache and templates${COLOR_INITIAL}\n"
ssh $DEST_USER@$DEST_HOST "cd $DEST_PATH ; php vendor/silverstripe/framework/cli-script.php dev/build \"flush=1\""

# Success!
printf "\n${COLOR_RED}> Deployment complete${COLOR_INITIAL}\n"
