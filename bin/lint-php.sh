#!/bin/bash

# Typography
COLOR_RED='\033[1;32m'
COLOR_INITIAL='\033[0m'

# Script paths
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Lint PHP files
printf "${COLOR_RED}> Linting PHP files${COLOR_INITIAL}\n"
pushd $DIR/../
vendor/bin/php-cs-fixer fix app/src
popd
