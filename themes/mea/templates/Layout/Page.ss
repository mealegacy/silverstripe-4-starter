<article class="content__wrapper">
    <div class="container">
        <%-- Title --%>
        <% if $Title %>
            <h1 class="content__title">$Title</h1>
        <% end_if %>

        <%-- Content --%>
        <% if $Content %>
            <div class="content__main">$Content</div>
        <% end_if %>

        <%-- Page/Dataobject Form --%>
        <% if $Form %>
            <div class="content__form">$Form</div>
        <% end_if %>
    </div>
</article>
