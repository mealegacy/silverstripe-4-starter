<div id="$Name" class="field form-check <% if $extraClass %>$extraClass<% end_if %>">
    $Field
	<% if $Message %><span class="message $MessageType">$Message</span><% end_if %>
</div>
