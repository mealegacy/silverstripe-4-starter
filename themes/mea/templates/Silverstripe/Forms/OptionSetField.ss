<% loop $Options %>
    <div class="form-check $Class">
        <label for="$ID" class="form-check-label">
            <input
                id="$ID"
                name="$Name"
                type="radio"
                class="form-check-input"
                value="$Value.ATT"
                <% if $isChecked %>checked<% end_if %>
                <% if $isDisabled %>disabled<% end_if %>
                <% if $Up.Required %>required<% end_if %>
            /> $Title
        </label>
    </div>
<% end_loop %>
