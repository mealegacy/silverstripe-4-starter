// JS Utilities

const JSUTILS = {};

JSUTILS.objToArray = (obj) => {
  return Object.keys(obj)
    .map((key) => obj[key]);
};

JSUTILS.debounce = (func, wait, immediate) => {
  let timeout;
  return function() {
    const context = this, args = arguments;
    const later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
};
